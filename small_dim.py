import numpy as np
from scipy.optimize import fsolve
from numpy.linalg import inv
import scipy.stats as stat

def error_th(a1, a2, B, y, tt, threshold):
    """Computes expected error for both classes with a given threshold and given labels"""
    return stat.norm.sf((threshold-np.dot(a1,y))/np.sqrt(y@B@y)), stat.norm.sf((np.dot(a2,y)-threshold)/np.sqrt(y@B@y))

def error_opti(a1, a2, B):
    """Computes expected error with optimal labels"""
    return stat.norm.sf(0.5*np.sqrt((a2-a1)@inv(B)@(a2-a1)))

def labels_opti(a1, a2, B):
    """Computes optimal label vector"""
    return inv(B)@(a2-a1)

def small_dim(data, hp, Mcal, tt):
    """Computes small dimensional quantities a1,a2,B associated to target task tt"""
    Lambda_tilde = hp.lbd_tilde()
    T = data.T
    c = data.p/np.sum(data.N)
    rho = data.N/np.sum(data.N)
    rho_bar = np.sum(rho,axis=1)
    v_rho = rho.flatten()
    eta = data.NL()/data.N
    eta_bar = np.sum(data.NL(),axis=1)/np.sum(data.N,axis=1)
    v_eta = eta.flatten()

    def f(x):
        return x - np.diag(Lambda_tilde+Lambda_tilde@inv(np.diag((T*c*(1-x))/rho_bar)-Lambda_tilde)@Lambda_tilde)/T
    delta = fsolve(f,np.diag(Lambda_tilde+Lambda_tilde@inv(np.diag(T*c/rho_bar)-Lambda_tilde)@Lambda_tilde)/T)
    delta_tilde = np.kron(1/(T*c*(1-delta)),[1,1])*v_rho
    delta_bar = np.sum(delta_tilde.reshape((T,2)),axis=1)
    Acal = Lambda_tilde+Lambda_tilde@inv(np.diag(1/delta_bar)-Lambda_tilde)@Lambda_tilde

    Theta_0 = np.kron(Acal,np.ones((2,2)))*Mcal
    if np.linalg.det((np.eye(2*T)-np.diag(delta_tilde)@Theta_0))==0:
        INV = np.array([[0,0],[0,0]])
    else:
        INV = inv(np.eye(2*T)-np.diag(delta_tilde)@Theta_0)
    Theta = Theta_0@INV

    gamma_t = (T*c*delta[tt])/rho_bar[tt]
    Gamma = np.kron(np.eye(T),np.ones((2,2)))
    xxx = np.zeros((T,T))
    xxx[0,0] = 1
    Gamma_t = np.kron(xxx,np.ones((2,2)))

    S_bar = np.zeros((T,T))
    for t1 in range(T):
        for t2 in range(T):
            S_bar[t1,t2] = Acal[t1,t2]**2/(T**2*c*(1-delta[t2])**2)
    S = S_bar@inv(np.eye(T)-np.diag(rho_bar)@S_bar)

    r_bar = rho_bar*S[tt]
    T_t = np.kron(np.diag(r_bar*eta_bar),np.ones((2,2)))
    r_bar[tt] += 1
    Omega_0_t = np.kron(Acal@(np.diag(r_bar))@Acal,np.ones((2,2)))*Mcal
    Omega_t = INV.T@Omega_0_t@INV
    # Omega_t = inv(np.eye(2*T)-Theta_0@np.diag(delta_tilde))@Omega_0_t@inv(np.eye(2*T)-np.diag(delta_tilde)@Theta_0)

    r = v_rho*np.kron(S[tt],[1,1])

    D_bar = np.zeros((2*T,2*T))
    D_tilde = np.zeros((2*T,2*T))
    L = data.labeled()
    for t in range(T):
        D_bar[2*t,2*t] = np.mean(data.degrees[t][0][0][L[t][0]])
        D_bar[2*t,2*t+1] = np.mean(data.degrees[t][0][1][L[t][0]])
        D_bar[2*t+1,2*t] = np.mean(data.degrees[t][1][0][L[t][1]])
        D_bar[2*t+1,2*t+1] = np.mean(data.degrees[t][1][1][L[t][1]])
        D1 = np.concatenate([data.degrees[t][0][0][L[t][0]],data.degrees[t][1][0][L[t][1]]])
        D_tilde[2*t,2*t] = np.mean(D1**2)
        D_tilde[2*t,2*t+1] = np.mean(D1*(1-D1))
        D_tilde[2*t+1,2*t] = np.mean(D1*(1-D1))
        D_tilde[2*t+1,2*t+1] = np.mean((1-D1)**2)

    B1 = D_bar.T@np.diag(v_eta)@(2*np.diag(delta_tilde)@(Theta-gamma_t*Gamma)-Gamma_t)@np.diag(r)@np.diag(v_eta)@D_bar
    B2 = D_bar.T@np.diag(v_eta)@np.diag(delta_tilde)@(Theta@np.diag(r)@Theta+Omega_t-gamma_t**2*Gamma_t)@np.diag(delta_tilde)@np.diag(v_eta)@D_bar
    B3 = T_t*D_tilde
    B = (B1+B2+B3)/(1-delta[tt])**2
    a = ((Theta-gamma_t*Gamma)@np.diag(delta_tilde)@np.diag(v_eta)@D_bar).T/(1-delta[tt])
    e1, e2 = np.zeros(2*T), np.zeros(2*T)
    e1[2*tt], e2[2*tt+1] = 1, 1
    a1, a2 = np.dot(a,e1), np.dot(a,e2)
    return a1, a2, B