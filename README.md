## Description
Experiments from the article "A Large Dimensional Analysis of Multi-task Semi-Supervised Learning" submitted to IEEE Transactions on Signal Processing.

## Jupyter Notebook files
Notebook that contains the experiments described in the article.

"test_stats.ipynb" : sanity check to validate the computations of the decision function's statistics.

"experiments.ipynb" : experiments on synthetic data

"real_data_experiments.ipynb" : experiments on real data, i.e. VGG features (see below)

## Backend files
Scripts that allows to run the algorithm.

"data.py" : generates the "Data" Class.

"hyperparameters.py" : generates the "Hyperparameters" class.

"small_dim_py" : computes the small dimensional quantities a1, a2 and B described in the article, and the theoretical error.

"optimal_bounds.py" : computes the optimal bounds from information theory.

## Other files
"DATA" : output data produced by the present code, and used in the article to display the figures.

"GAN_VGG" : VGG features extracted from GAN-generated images, used as real data in the experiments.

"gen_data.py" : script that generates the VGG features

## License
CC-BY-SA

## Project status
No further development is planned.
