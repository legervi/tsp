import numpy as np
from scipy.optimize import fsolve
from numpy.linalg import inv
from scipy.optimize import minimize_scalar
from math import isnan
from small_dim import *

class HyperParameters:
    """Hyperparameters, i.e. Lambda and alpha"""
    def __init__(self,T):
        self.lbd = np.ones((T,T))
        self.alpha=1
        self.alpha0=1

    def lbd_tilde(self):
        return self.lbd/self.alpha

    def compute_lambda(self,data,method,MU=0):
        """Set the value of Mcal associated to the data

        method='true' : Use ground truth means MU
        method='estimate' : Use the estimated mean
        """
        if method=='true':
            for t1 in range(data.T):
                for t2 in range(data.T):
                    self.lbd[t1,t2] = np.abs(np.dot(MU[t1,0]-MU[t1,1],MU[t2,0]-MU[t2,1]))/(np.linalg.norm(MU[t1,0]-MU[t1,1],ord=2)*(np.linalg.norm(MU[t2,0]-MU[t2,1],ord=2)))

        elif method=='estimate':
            for t1 in range(data.T):
                X_0 = data.samples[t1][0][:,data.reliable()[t1][0]]
                X_1 = data.samples[t1][1][:,data.reliable()[t1][1]]
                n_0, n_1 = X_0.shape[1], X_1.shape[1]
                m_0_0 = np.mean(X_0[:,0:n_0//2],axis=1)-np.mean(X_1[:,0:n_1//2],axis=1)
                m_0_1 = np.mean(X_0[:,n_0//2::],axis=1)-np.mean(X_1[:,n_1//2::],axis=1)
                m_0 = np.mean(X_0,axis=1)-np.mean(X_1,axis=1)
                for t2 in range(data.T):
                    X_0 = data.samples[t2][0][:,data.reliable()[t2][0]]
                    X_1 = data.samples[t2][1][:,data.reliable()[t2][1]]
                    n_0, n_1 = X_0.shape[1], X_1.shape[1]
                    m_1_0 = np.mean(X_0[:,0:n_0//2],axis=1)-np.mean(X_1[:,0:n_1//2],axis=1)
                    m_1_1 = np.mean(X_0[:,n_0//2::],axis=1)-np.mean(X_1[:,n_1//2::],axis=1)
                    m_1 = np.mean(X_0,axis=1)-np.mean(X_1,axis=1)
                    self.lbd[t1,t2] = np.dot(m_0,m_1)/(np.sqrt(np.dot(m_0_0,m_0_1))*np.sqrt(np.dot(m_1_0,m_1_1)))
                    if isnan(self.lbd[t1,t2]):
                        self.lbd[t1,t2] = 1
            for t in range(data.T):
                self.lbd[t,t] = 1
        elif method=='ord1':
            for t1 in range(data.T):
                for t2 in range(data.T):
                    self.lbd[t1,t2] = np.sqrt(np.abs(np.dot(MU[t1,0]-MU[t1,1],MU[t2,0]-MU[t2,1]))/(np.linalg.norm(MU[t1,0]-MU[t1,1],ord=2)*(np.linalg.norm(MU[t2,0]-MU[t2,1],ord=2))))
        elif method=='ord2':
            for t1 in range(data.T):
                for t2 in range(data.T):
                    self.lbd[t1,t2] = np.abs(np.dot(MU[t1,0]-MU[t1,1],MU[t2,0]-MU[t2,1]))/(np.linalg.norm(MU[t1,0]-MU[t1,1],ord=2)*(np.linalg.norm(MU[t2,0]-MU[t2,1],ord=2)))
        elif method=='ord4':
            for t1 in range(data.T):
                for t2 in range(data.T):
                    self.lbd[t1,t2] = (np.abs(np.dot(MU[t1,0]-MU[t1,1],MU[t2,0]-MU[t2,1]))/(np.linalg.norm(MU[t1,0]-MU[t1,1],ord=2)*(np.linalg.norm(MU[t2,0]-MU[t2,1],ord=2))))**2
        else:
            print("Invalid value of method")

    def compute_alpha0(self,data):
        """Set the value of self.alpha0 to the minimum ensuring a good conditionment of delta"""
        T = data.T
        c = data.p/np.sum(data.N)
        rho = data.N/np.sum(data.N)
        lbd = self.lbd
        rho_bar = np.sum(rho,axis=1)
        t0 = np.argmax(rho_bar)
        s_bar = 1/rho_bar
        vaps, P = np.linalg.eig(np.diag(rho_bar)@lbd)
        y_tilde_0 = (1+np.sqrt(c/rho_bar[t0]))*np.max(vaps)
        if T>1:
            for i in range(10):
                M = np.diag(1/s_bar)@lbd
                def g(x):
                    return (M@inv(x*np.eye(T)-M)@inv(x*np.eye(T)-M)@M)[t0,t0]-rho_bar[t0]/c
                y_tilde = fsolve(g,y_tilde_0)[0]
                def h(x):
                    u = np.concatenate((x[0:t0],[y_tilde/rho_bar[t0]],x[t0::]))
                    v = np.diag(lbd@inv(np.diag(u)-lbd)@lbd)+rho_bar*u/c
                    return v[0:T-1]-v[1:T]
                x_bar = fsolve(h,np.zeros(T-1))
                y_bar = np.concatenate((x_bar[0:t0],[y_tilde/rho_bar[t0]],x_bar[t0::]))
                s_bar = y_bar/y_tilde
        else:
            M = np.diag(1/s_bar)@lbd
            def g(x):
                return (M@inv(x*np.eye(T)-M)@inv(x*np.eye(T)-M)@M)[t0,t0]-rho_bar[t0]/c
            y_tilde = fsolve(g,y_tilde_0)[0]
            y_bar = np.array([y_tilde/rho_bar[t0]])
        self.alpha0 = (lbd+lbd@inv(np.diag(y_bar)-lbd)@lbd)[t0,t0]/T+(rho_bar*y_bar)[t0]/(T*c)

    def optim_alpha(self, data, Mcal, tt, tol=1e-3):
        """Set the value of self.alpha that minimizes "error_opti"

        tol : maximum relative error acceptable
        """
        def sd(data, self, Mcal, tt):
            return small_dim(data, self, Mcal, tt)

        def funk(alpha):
            self.alpha = alpha
            a1, a2, B = sd(data, self, Mcal, tt)
            e = error_opti(a1,a2,B)
            if isnan(e):
                return 0.5
            else:
                return e

        x1, x2, x3 = 1.5*self.alpha0, 2*self.alpha0, 3*self.alpha0
        e1, e2, e3 = funk(x1), funk(x2), funk(x3)
        while e2>e1 or e2>e3:
            if e2>e1:
                x1, x2, x3 = ((x1/self.alpha0+1)/2)*self.alpha0, x1, x2
                e1, e2, e3 = funk(x1), e1, e2
            elif e2>e3:
                x1, x2, x3 = x2, x3, (2*x3/self.alpha0-1)*self.alpha0
                e1, e2, e3 = e2, e3, funk(x3)
        self.alpha = minimize_scalar(funk, bracket=[x1,x2,x3], method='golden', tol=tol).x