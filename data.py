import numpy as np
from numpy.linalg import inv

class Data:
    """Data

    T : number of tasks
    m : number of classes
    p : dimension of data
    N : (T,m)-array with the number of data
    samples : (T,m)-list with (p,N[t,j])-arrays of data
    degrees : (T,m)-list with (2,N[t,j])-arrays of probabilities
    """
    def __init__(self,p,N):
        self.N = np.array(N)
        self.T = len(N)
        self.m = len(N[0])
        self.p = p
        self.samples = [[0 for j in range(self.m)] for t in range(self.T)]
        for t in range(self.T):
            for j in range(self.m):
                self.samples[t][j] = np.zeros((self.p,self.N[t,j]))
        self.degrees = [[0 for j in range(self.m)] for t in range(self.T)]
        for t in range(self.T):
            for j in range(self.m):
                self.degrees[t][j] = np.zeros((2,self.N[t,j]))

    def create_degrees(self,NL,NU):
        """Create degrees in the easy case of purely labeled or unlabeled data"""
        NL = np.array(NL)
        NU = np.array(NU)
        if (NL+NU==self.N).all():
            for t in range(self.T):
                self.degrees[t][0] = np.array([np.concatenate([np.ones(NL[t,0]),np.zeros(NU[t,0])]),np.concatenate([np.zeros(NL[t,0]),np.zeros(NU[t,0])])])
                self.degrees[t][1] = np.array([np.concatenate([np.zeros(NL[t,1]),np.zeros(NU[t,1])]),np.concatenate([np.ones(NL[t,1]),np.zeros(NU[t,1])])])
        else:
            print("Uncompatible values of NL and NU")

    def create_gaussian(self,MU):
        """Generate gaussian isotropic data for a given set of means MU"""
        for t in range(self.T):
            for j in range(self.m):
                self.samples[t][j] = np.random.multivariate_normal(MU[t,j],np.eye(self.p),size=self.N[t,j]).T

    def create_gaussian_2(self,MU):
        """Generate gaussian isotropic data for a given set of means MU"""
        for t in range(self.T):
            for j in range(self.m):
                self.samples[t][j] = np.random.multivariate_normal(MU[t,j],2*np.eye(self.p),size=self.N[t,j]).T

    def center(self):
        """Center data task-wise"""
        for t in range(self.T):
            nt = np.sum(self.N[t])
            Vt = np.zeros(self.p)
            for j in range(self.m):
                Vt += np.sum(self.samples[t][j],axis=1)
            Vt = np.reshape(Vt,(self.p,1))/nt
            for j in range(self.m):
                self.samples[t][j] -= Vt@np.ones((1,self.N[t,j]))

    def normalize(self):
        """Normalize the data so that the covariance magnitude is 1"""
        for t in range(self.T):
            X0, X1 = self.samples[t][0], self.samples[t][1]
            n0 = X0.shape[1]
            Xt = np.concatenate([X0,X1],axis=1)
            Xt = Xt/np.sqrt(np.sum(Xt@Xt.T)/(np.sum(self.N[t])*self.p))
            self.samples[t][0] = Xt[:,0:n0]
            self.samples[t][1] = Xt[:,n0::]

    def Z(self):
        """Return the data as Z format"""
        Z = np.zeros((self.T*self.p,np.sum(self.N)))
        for t in range(self.T):
            Z[t*self.p:(t+1)*self.p,np.sum(self.N[0:t]):np.sum(self.N[0:t+1])] = np.hstack(self.samples[t])
        return Z

    def NU(self):
        """Return the number of unlabeled data NU (with the shape of N)"""
        NU = np.zeros((self.T,self.m))
        U = self.unknown()
        for t in range(self.T):
            for j in range(self.m):
                NU[t,j] = np.sum(U[t][j])
        return NU

    def NL(self):
        """Return the number of labeled data NL (with the shape of N)"""
        return self.N-self.NU()

    def Mcal(self,method,MU=0):
        """Return the value of Mcal

        method='true' : Use ground truth means MU
        method='estimate' : Use the estimator with splitted samples
        """
        Mcal = np.zeros((self.m*self.T,self.m*self.T))
        if method=='true':
            MUc = MU.copy()
            for t in range(self.T):
                vt = np.zeros(self.p)
                for j in range(self.m):
                    vt += (self.N[t,j])*MU[t,j]
                vt /= np.sum(self.N[t])
                for j in range(self.m):
                    MUc[t,j] -= vt
            M = np.reshape(MUc,(self.m*self.T,self.p)).T
            Mcal = M.T@M
        elif method=='estimate':
            for t1 in range(self.T):
                for j1 in range(self.m):
                    for t2 in range(self.T):
                        for j2 in range(self.m):
                            Mcal[self.m*t1+j1,self.m*t2+j2] = np.dot(np.mean(self.samples[t1][j1][:,self.reliable()[t1][j1]],axis=1),np.mean(self.samples[t2][j2][:,self.reliable()[t2][j2]],axis=1))
            for t in range(self.T):
                for j in range(self.m):
                    Xlt = self.samples[t][j][:,self.reliable()[t][j]]
                    n = len(Xlt[0])
                    Mcal[self.m*t+j,self.m*t+j] = np.dot(np.mean(Xlt[:,0:n//2],axis=1),np.mean(Xlt[:,n//2::],axis=1))
        else:
            print("Invalid value of method")
        return Mcal

    def scores(self, hp, y, tt):
        """Computes the full score vectors of the 2 classes for target task tt"""
        A = np.kron(hp.lbd_tilde(),np.eye(self.p))
        Z = self.Z()
        Q_prime = inv(np.eye(np.sum(self.N))-(Z.T@A@Z)/(self.T*self.p))
        Y = np.zeros(np.sum(self.N))
        for t in range(self.T):
            Y[np.sum(self.N[0:t]):np.sum(self.N[0:t])+self.N[t,0]] = y[2*t]*self.degrees[t][0][0] + y[2*t+1]*self.degrees[t][0][1]
            Y[np.sum(self.N[0:t])+self.N[t,0]:np.sum(self.N[0:t+1])] = y[2*t]*self.degrees[t][1][0] + y[2*t+1]*self.degrees[t][1][1]
        # print(Y)
        F = Q_prime@Y
        # print(F)
        Ft = F[np.sum(self.N[0:tt]):np.sum(self.N[0:tt+1])]
        U = self.unknown()
        return [Ft[:self.N[tt,0]][U[tt][0]], Ft[self.N[tt,0]::][U[tt][1]]]

    def scores_2(self, hp, y, tt):
        """Computes the full score vectors of the 2 classes for target task tt"""
        A = np.kron(hp.lbd_tilde(),np.eye(self.p))
        Z = self.Z()
        #Q_prime = inv(np.eye(np.sum(self.N))-(Z.T@A@Z)/(self.T*self.p))
        Y = np.zeros(np.sum(self.N))
        for t in range(self.T):
            Y[np.sum(self.N[0:t]):np.sum(self.N[0:t])+self.N[t,0]] = y[2*t]*self.degrees[t][0][0] + y[2*t+1]*self.degrees[t][0][1]
            Y[np.sum(self.N[0:t])+self.N[t,0]:np.sum(self.N[0:t+1])] = y[2*t]*self.degrees[t][1][0] + y[2*t+1]*self.degrees[t][1][1]
        F = 1/(self.T*self.p*hp.alpha)*Z.T@Z@Y
        Ft = F[np.sum(self.N[0:tt]):np.sum(self.N[0:tt+1])]
        U = self.unknown()
        return [Ft[:self.N[tt,0]][U[tt][0]], Ft[self.N[tt,0]::][U[tt][1]]]

    def error_emp(self, hp, y, tt, threshold):
        """Computes prediction error for both classes with a given threshold in the case unlabeled data is ordered"""
        F1,F2 = self.scores(hp, y, tt)
        U = self.unknown()
        return np.mean(F1>threshold), np.mean(F2<threshold)

    def reliable(self):
        R = [[0 for j in range(self.m)] for t in range(self.T)]
        for t in range(self.T):
            for j in range(self.m):
                R[t][j] = self.degrees[t][j][j]==1
        return R

    def unknown(self):
        U = [[0 for j in range(self.m)] for t in range(self.T)]
        for t in range(self.T):
            U[t][0] = (self.degrees[t][0][0]==0)*(self.degrees[t][0][1]==0)
            U[t][1] = (self.degrees[t][1][0]==0)*(self.degrees[t][1][1]==0)
        return U

    def labeled(self):
        L = [[0 for j in range(self.m)] for t in range(self.T)]
        U = self.unknown()
        for t in range(self.T):
            for j in range(self.m):
                L[t][j] = np.array(1-U[t][j],dtype=bool)
        return L