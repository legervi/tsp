import argparse
import numpy as np
import torch
import torchvision.models as models
import torchvision.transforms as transforms
from tqdm import tqdm
from pytorch_pretrained_biggan import (BigGAN, one_hot_from_names, truncated_noise_sample, save_as_images)
from skimage.transform import resize

current_class = 'boxer'
n = 500

path = 'GAN_VGG/'+current_class+'/'+current_class+"_{}"

model_gan = BigGAN.from_pretrained('biggan-deep-256')
model_vgg = models.vgg19(pretrained=True)
model_gan.eval()
model_vgg.eval()

model_vgg.classifier = model_vgg.classifier[0] # remove last layers

features_file = open('GAN_VGG/'+current_class+'_features.csv', 'w')

scale = transforms.Resize((224, 224)) # VGG input size
norm = transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
pil = transforms.ToPILImage()

class_vector = one_hot_from_names(current_class)
class_vector = torch.from_numpy(class_vector)

for ite in tqdm(range(n)):
    noise_vector = truncated_noise_sample(truncation=1.0, batch_size=1)
    noise_vector = torch.from_numpy(noise_vector)

    with torch.no_grad():
        image = model_gan(noise_vector, class_vector, 1.0)
        im = resize(np.array(image[0]), (3,224,224))
        image = torch.from_numpy(np.array([im]))
        features = model_vgg(image)

    # Save results
    save_as_images(image, file_name=path.format(ite))
    np.savetxt(features_file, features.numpy())

features_file.close()